# Upgrading LiberaForms

There are two ways to upgrade LiberaForms. Script assisted, and Manual.

## Script assisted upgrade

upgrade.py will first make a rollback copy of your LiberaForms installation, then pull the new code and upgrade.

```
cd LiberaForms
source venv/bin/activate
python upgrade.py
```

### Restart LiberaForms
```
supervisorctl restart LiberaForms
```

## Manual upgrade

### Pull the new code
```
cd LiberaForms
git pull origin main
source venv/bin/activate
pip install --upgrade setuptools
pip install -r ./requirements.txt
```

### Upgrade database schema
First, **backup your database**. Then run the mirgate_db.py script.
```
source venv/bin/activate
python migrate_db.py
```

### Restart LiberaForms
```
supervisorctl restart LiberaForms
```

### Permissions
Make sure the user who runs LiberaForms has write permission
```
chown -R www-data ./liberaforms/static/images
```
